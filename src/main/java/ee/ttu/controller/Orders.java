package ee.ttu.controller;

import ee.ttu.dao.OrderDAO;
import ee.ttu.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("orders")
public class Orders {

    @Autowired
    private OrderDAO orderDAO;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Order list(@PathVariable("id") Long id) {
        return orderDAO.getOrderById(id);
    }

    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public Order save(@RequestBody @Valid Order order) {
        return orderDAO.saveOrder(order);
    }
}
