package ee.ttu.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Order {

    private Long id;

    @Valid
    private List<OrderRow> orderRows;

    private String orderNumber;

    public void add(OrderRow orderRow) {
        if (orderRows == null) {
            orderRows = new ArrayList<>();
        }
        orderRows.add(orderRow);
    }


}
