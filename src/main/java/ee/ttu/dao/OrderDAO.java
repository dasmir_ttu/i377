package ee.ttu.dao;

import ee.ttu.model.Order;

import java.util.List;

public interface OrderDAO {

    Order getOrderById(Long id);

    List<Order> getAllOrders();

    Order saveOrder(Order order);

//    void saveOrderRows(long orderId, List<OrderRow> orderRows);

    void deleteOrder(long orderID);

    void deleteOrders();
}
