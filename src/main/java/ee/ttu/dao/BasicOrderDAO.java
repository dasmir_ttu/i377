package ee.ttu.dao;

import ee.ttu.model.Order;
import ee.ttu.model.OrderRow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

@Primary
@Repository("orderDAO")
public class BasicOrderDAO implements OrderDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public Order getOrderById(Long id) {

        PreparedStatementCreator preparedStatementCreator = connection -> {
                PreparedStatement ps = connection.prepareStatement(
                    "SELECT o.id, o.order_number, o_row.item_name, o_row.quantity, o_row.price FROM orders o LEFT JOIN order_row o_row on o.id = o_row.order_id WHERE id = ?");
                ps.setLong(1, id);
            return ps;
        };

        PreparedStatementCallback<Order> preparedStatementCallback =
                preparedStatement -> {
                    ResultSet resultSet = preparedStatement.executeQuery();

                    Map<Long, Order> orderMap = new HashMap<>();
                    while (resultSet.next()) {
                        if (orderMap.get(id) == null) {
                            Order order = new Order();
                            order.setId(resultSet.getLong("ID"));
                            order.setOrderNumber(resultSet.getString("ORDER_NUMBER"));
                            orderMap.put(id, order);
                        }

                        Order currentOrder = orderMap.get(id);
                        OrderRow orderRow = new OrderRow();
                        orderRow.setItemName(resultSet.getString("ITEM_NAME"));
                        orderRow.setQuantity(resultSet.getInt("QUANTITY"));
                        orderRow.setPrice(resultSet.getInt("PRICE"));

                        currentOrder.add(orderRow);
                    }

                    return orderMap.get(id);
                };
        return jdbcTemplate.execute(preparedStatementCreator, preparedStatementCallback);
    }

    public List<Order> getAllOrders() {

        PreparedStatementCreator preparedStatementCreator = connection -> {
            PreparedStatement ps = connection.prepareStatement(
                "SELECT o.id, o.order_number, o_row.item_name, o_row.quantity, o_row.price FROM orders o LEFT JOIN order_row o_row on o.id = o_row.order_id");
            return ps;
        };

        PreparedStatementCallback<List<Order>> preparedStatementCallback =
                preparedStatement -> {
                    ResultSet resultSet = preparedStatement.executeQuery();

                    Map<Long, Order> orderMap = new LinkedHashMap<>();
                    while (resultSet.next()) {
                        long id = resultSet.getLong("ID");
                        if (orderMap.get(id) == null) {
                            Order order = new Order();
                            order.setId(id);
                            order.setOrderNumber(resultSet.getString("ORDER_NUMBER"));
                            orderMap.put(id, order);
                        }

                        Order currentOrder = orderMap.get(id);
                        OrderRow orderRow = new OrderRow();
                        orderRow.setItemName(resultSet.getString("ITEM_NAME"));
                        orderRow.setQuantity(resultSet.getInt("QUANTITY"));
                        orderRow.setPrice(resultSet.getInt("PRICE"));

                        currentOrder.add(orderRow);
                    }

                    return new ArrayList<>(orderMap.values());
                };

        return jdbcTemplate.execute(preparedStatementCreator, preparedStatementCallback);
    }

    public Order saveOrder(Order order) {

        GeneratedKeyHolder holder = new GeneratedKeyHolder();


        PreparedStatementCreator preparedStatementCreator = connection -> {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO orders VALUES (NEXT VALUE FOR seq1, ?)", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, order.getOrderNumber());
            return preparedStatement;
        };

        jdbcTemplate.update(preparedStatementCreator, holder);
        order.setId(holder.getKey().longValue());

        if (order.getOrderRows() != null) {
            saveOrderRows(order.getId(), order.getOrderRows());
        }

        return order;
    }

    private void saveOrderRows(long orderId, List<OrderRow> orderRows) {

        PreparedStatementCreator preparedStatementCreator = connection -> {
        PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO order_row (item_name, quantity, price, order_id) VALUES (?, ?, ?, ?)");
            for (OrderRow orderRow : orderRows) {
                preparedStatement.setString(1, orderRow.getItemName());
                preparedStatement.setInt(2, orderRow.getQuantity());
                preparedStatement.setInt(3, orderRow.getPrice());
                preparedStatement.setLong(4, orderId);
                preparedStatement.addBatch();
            }
            return preparedStatement;
        };

        PreparedStatementCallback<Object> preparedStatementCallback =
                preparedStatement -> {
                    preparedStatement.executeBatch();
                    return null;
                };

        jdbcTemplate.execute(preparedStatementCreator, preparedStatementCallback);
    }

    public void deleteOrder(long orderID) {

        PreparedStatementCreator preparedStatementCreator = connection -> {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM orders WHERE id = ?");
            preparedStatement.setLong(1, orderID);
            return preparedStatement;
        };

        PreparedStatementCallback<Object> preparedStatementCallback =
                preparedStatement -> {
                    preparedStatement.executeUpdate();
                    return null;
                };

        jdbcTemplate.execute(preparedStatementCreator, preparedStatementCallback);
    }

    public void deleteOrders() {

        PreparedStatementCreator preparedStatementCreator = connection -> connection.prepareStatement("DELETE FROM orders");

        PreparedStatementCallback<Object> preparedStatementCallback =
                preparedStatement -> {
                    preparedStatement.executeUpdate();
                    return null;
                };

        jdbcTemplate.execute(preparedStatementCreator, preparedStatementCallback);
    }
}
