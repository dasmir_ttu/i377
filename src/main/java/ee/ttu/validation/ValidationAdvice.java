package ee.ttu.validation;

import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

@ControllerAdvice
public class ValidationAdvice {

    @ExceptionHandler
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ValidationErrors handleMethodArgumentNotValid(
            MethodArgumentNotValidException exception) {
        ValidationErrors validationErrors = new ValidationErrors();

        List<FieldError> errors = exception.getBindingResult().getFieldErrors();

        errors.forEach(validationErrors::addError);

        return validationErrors;
    }
}